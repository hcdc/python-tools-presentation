# python-tools-presentation

presentation about modern tools for software development with python

## Installation

The presentation is in the [python-tools-presentation.ipynb](python-tools-presentation.ipynb)
notebook. 

### Remote without installation
You can run these notebooks without installation using mybinder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.hzdr.de%2Fhcdc%2Fpython-tools-presentation/HEAD)

### Local installation

You can run it with having jupyter notebook installed (and being on
a linux system). Just run `jupyter notebook` to start the notebook server.

It requires:

- python>=3.7
- pip

and more (see [requirements.txt](requirements.txt)).

You can install the other requirements via `pip install -r requirements.txt`
